# Concurrency Project

The goal of this project is to experiment with different ways of using multithreading
to find the smallest number in an array.

## Algorithms

Here is a summary of each of the algorithms that were tested

### No Lock

* `smallestNumber` is stored as an int and is not given the `volatile` keyword
* Each thread accesses and potentially `smallestNumber` at each of their iterations

### Lock

* `smallestNumber` is stored as an int and is not given the `volatile` keyword
* Each thread accesses and potentially `smallestNumber` at each of their iterations
* There is a lock placed on `smallestNumber` so that only one thread can interact 
  with it at a time

### Atomic

* `smallestNumber` is stored as an `AtomicInteger` and is given the `final` keyword
* Each thread accesses and potentially `smallestNumber` at each of their iterations
* Due to the atomic nature of `smallestNumber`, no additional locking is necessary

### Spread and Gather

* There is a proper name for this type of algorithm, but I can't remember what it is
* `smallestNumber` is stored as an `AtomicInteger` and is given the `final` keyword
* Each thread has a `localSmallestNumber` which is stored as an `int`
* Each thread only interacts with `smallestNumber` after iteration is complete
  and `localSmallestNumber` is the smallest number in the thread's assigned area

## Experiments

Each of the methods was tested using the same 100 million element array.
This array was filled with random numbers using the following code snippet:

```
Random rand = new Random(0);
for (int i = 0; i < numbers.length; i++)
{
	numbers[i] = Math.abs(rand.nextInt());
}
```

## Results

* The fastest algorithm by far is the Spread and Gather method.
  This is because there are very few occasions when multiple
  threads need to interact with the same resource.
  
* The second fastest algorithm is the one that doesn't use a lock,
  but that method often gives an incorrect solution.
  
* The third fastest algorithm is the atomic method.
  This method isn't as fast as the one that doesn't use a lock,
  but it always generates the correct solution.
  
* The slowest algorithm is the one that uses a lock.
  While it always gives the correct solution, it is much slower
  than the atomic method.


## Smallest Number Using a Lock
|Number of Threads|Time Passed (ms)|Result|
|---|---|---|
|1|1330|2|
|2|3743|2|
|4|6030|2|
|8|5855|2|
|16|5475|2|
|32|6098|2|

## Smallest Number Without Using a Lock
|Number of Threads|Time Passed (ms)|Result|
|---|---|---|
|1|83|2|
|2|45|117|
|4|36|18|
|8|38|2|
|16|50|198|
|32|51|18|

## Smallest Number Using an AtomicInteger
|Number of Threads|Time Passed (ms)|Result|
|---|---|---|
|1|623|2|
|2|1174|2|
|4|1349|2|
|8|1519|2|
|16|2910|2|
|32|1594|2|

## Smallest Number Using a Spread and Gather Strategy
|Number of Threads|Time Passed (ms)|Result|
|---|---|---|
|1|83|2|
|2|45|2|
|4|21|2|
|8|13|2|
|16|14|2|
|32|13|2|

