import java.util.concurrent.atomic.AtomicInteger;

public class SmallestNumberGather extends SmallestNumber
{
	private final AtomicInteger smallestNumber;

	public SmallestNumberGather(int[] numbers, int numThreads)
	{
		super(numbers, numThreads);

		smallestNumber = new AtomicInteger(Integer.MAX_VALUE);
	}

	public int run()
	{
		int currentIndex = 0;
		int intervalLength = numbers.length / numThreads;
		for(int i = 0; i < numThreads-1; i++)
		{
			new Thread(currentIndex, currentIndex + intervalLength).start();
			currentIndex += intervalLength;
		}
		new Thread(currentIndex, numbers.length).start();

		synchronized (mainLock)
		{
			try
			{
				mainLock.wait();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}

		return smallestNumber.get();
	}

	private class Thread extends java.lang.Thread
	{
		private final int startIndex;
		private final int endIndex;

		private Thread(int startIndex, int endIndex)
		{
			this.startIndex = startIndex;
			this.endIndex = endIndex;
		}

		@Override
		public void run()
		{
			int localSmallestNumber = numbers[startIndex];
			for(int i = startIndex+1; i < endIndex; i++) {
				localSmallestNumber = Math.min(numbers[i], localSmallestNumber);
			}

			smallestNumber.accumulateAndGet(localSmallestNumber, Math::min);

			if(completedTasks.incrementAndGet() == numThreads) {
				synchronized (mainLock)
				{
					mainLock.notify();
				}
			}
		}
	}
}
