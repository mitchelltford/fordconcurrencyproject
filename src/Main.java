import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Main
{
	private static final int[] numbers = new int[100000000];

	static
	{
		Random rand = new Random(0);
		for (int i = 0; i < numbers.length; i++)
		{
			numbers[i] = Math.abs(rand.nextInt());
		}
	}

	public static void main(String[] args)
	{
		runTests();
	}

	private static void runTests()
	{
		try (FileWriter fileWriter = new FileWriter("results.md", false))
		{
			fileWriter.write("## Smallest Number Using a Lock\n");
			fileWriter.write("|Number of Threads|Time Passed (ms)|Result|\n");
			fileWriter.write("|---|---|---|\n");
			for(int numThreads = 1; numThreads <= 32 ; numThreads *= 2)
			{
				long startTime = System.currentTimeMillis();
				int result = new SmallestNumberLock(numbers, numThreads).run();
				long endTime = System.currentTimeMillis();

				fileWriter.write(String.format("|%d|%d|%d|\n", numThreads, endTime-startTime, result));
			}
			fileWriter.write("\n");

			fileWriter.write("## Smallest Number Without Using a Lock\n");
			fileWriter.write("|Number of Threads|Time Passed (ms)|Result|\n");
			fileWriter.write("|---|---|---|\n");
			for(int numThreads = 1; numThreads <= 32 ; numThreads *= 2)
			{
				long startTime = System.currentTimeMillis();
				int result = new SmallestNumberNoLock(numbers, numThreads).run();
				long endTime = System.currentTimeMillis();

				fileWriter.write(String.format("|%d|%d|%d|\n", numThreads, endTime-startTime, result));
			}
			fileWriter.write("\n");

			fileWriter.write("## Smallest Number Using an AtomicInteger\n");
			fileWriter.write("|Number of Threads|Time Passed (ms)|Result|\n");
			fileWriter.write("|---|---|---|\n");
			for(int numThreads = 1; numThreads <= 32 ; numThreads *= 2)
			{
				long startTime = System.currentTimeMillis();
				int result = new SmallestNumberAtomic(numbers, numThreads).run();
				long endTime = System.currentTimeMillis();

				fileWriter.write(String.format("|%d|%d|%d|\n", numThreads, endTime-startTime, result));
			}
			fileWriter.write("\n");

			fileWriter.write("## Smallest Number Using a Spread and Gather Strategy\n");
			fileWriter.write("|Number of Threads|Time Passed (ms)|Result|\n");
			fileWriter.write("|---|---|---|\n");
			for(int numThreads = 1; numThreads <= 32 ; numThreads *= 2)
			{
				long startTime = System.currentTimeMillis();
				int result = new SmallestNumberGather(numbers, numThreads).run();
				long endTime = System.currentTimeMillis();

				fileWriter.write(String.format("|%d|%d|%d|\n", numThreads, endTime-startTime, result));
			}
			fileWriter.write("\n");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
