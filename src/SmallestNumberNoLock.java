public class SmallestNumberNoLock extends SmallestNumber
{
	private int smallestNumber = Integer.MAX_VALUE;

	public SmallestNumberNoLock(int[] numbers, int numThreads)
	{
		super(numbers, numThreads);
	}

	@Override
	protected int run()
	{
		int currentIndex = 0;
		int intervalLength = numbers.length / numThreads;
		for(int i = 0; i < numThreads-1; i++)
		{
			new Thread(currentIndex, currentIndex + intervalLength).start();
			currentIndex += intervalLength;
		}
		new Thread(currentIndex, numbers.length).start();

		synchronized (mainLock)
		{
			try
			{
				mainLock.wait();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}

		return smallestNumber;
	}

	private class Thread extends java.lang.Thread
	{
		private final int start;
		private final int end;

		public Thread(int start, int end)
		{
			this.start = start;
			this.end = end;
		}

		@Override
		public void run()
		{
			for(int i = start; i < end; i++)
			{
				smallestNumber = Math.min(smallestNumber, numbers[i]);
			}

			// This thread is done with it's task
			// Check to see if all threads are done
			if(completedTasks.incrementAndGet() == numThreads)
			{
				synchronized (mainLock)
				{
					mainLock.notify();
				}
			}
		}
	}
}
