import java.util.concurrent.atomic.AtomicInteger;

public abstract class SmallestNumber
{
	protected final int[] numbers;
	protected final int numThreads;

	/**
	 * The number of threads that have completed their tasks.
	 */
	protected final AtomicInteger completedTasks;

	/**
	 * A lock object that prevents {@link SmallestNumber#run()} from returning until
	 * all threads have completed their tasks.
	 */
	protected final Object mainLock;

	public SmallestNumber(int[] numbers, int numThreads)
	{
		this.numbers = numbers;
		if (numbers == null || numbers.length == 0)
		{
			throw new IllegalArgumentException("numbers must a non-null and it must have a length greater than 0");
		}

		this.numThreads = numThreads;
		if (numThreads <= 0)
		{
			throw new IllegalArgumentException("numThreads must be greater than 0");
		}

		completedTasks = new AtomicInteger(0);

		mainLock = new Object();

	}

	protected abstract int run();
}
